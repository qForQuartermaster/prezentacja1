package abm.ant8.doprezentacji

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import org.jetbrains.anko.doAsync
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import java.net.URL

/**
 * Instrumentation test, which will execute on an Android device.

 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    @Throws(Exception::class)
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()

        assertEquals("abm.ant8.doprezentacji", appContext.packageName)
    }

    @Test
    fun shouldUrlBeDownloaded() {

        var response: String? = null
        doAsync {
            response = URL("api.stackexchange.com/2.2/search" +
                    "?page=1&pagesize=2" +
                    "&order=desc&sort=activity" +
                    "&tagged=anko&intitle=kotlin&site=stackoverflow").readText()
        }
        val correctResponse = "{" +
                "\"items\": [" +
                "{" +
                "\"tags\": [" +
                "\"android\"," +
                "\"kotlin\"," +
                "\"anko\"" +
                "]," +
                "\"owner\": {" +
                "\"reputation\": 735," +
                "\"user_id\": 3806331," +
                "\"user_type\": \"registered\"," +
                "\"accept_rate\": 95," +
                "\"profile_image\": \"https://www.gravatar.com/avatar/ab7f221d4245947fd3260c364ffd0645?s=128&d=identicon&r=PG&f=1\"," +
                "\"display_name\": \"user3806331\"," +
                "\"link\": \"http://stackoverflow.com/users/3806331/user3806331\"" +
                "}," +
                "\"is_answered\": false," +
                "\"view_count\": 72," +
                "\"answer_count\": 0," +
                "\"score\": 1," +
                "\"last_activity_date\": 1480342298," +
                "\"creation_date\": 1480329706," +
                "\"last_edit_date\": 1480342298," +
                "\"question_id\": 40842287," +
                "\"link\": \"http://stackoverflow.com/questions/40842287/kotlin-anko-custom-view-parent-scope\"," +
                "\"title\": \"Kotlin Anko Custom View Parent scope\"" +
                "}," +
                "{" +
                "\"tags\": [" +
                "\"kotlin\"," +
                "\"anko\"" +
                "]," +
                "\"owner\": {" +
                "\"reputation\": 1214," +
                "\"user_id\": 1347816," +
                "\"user_type\": \"registered\"," +
                "\"accept_rate\": 79," +
                "\"profile_image\": \"https://i.stack.imgur.com/Ik5jP.jpg?s=128&g=1\"," +
                "\"display_name\": \"Mel  Adane\"," +
                "\"link\": \"http://stackoverflow.com/users/1347816/mel-adane\"" +
                "}," +
                "\"is_answered\": true," +
                "\"view_count\": 36," +
                "\"answer_count\": 2," +
                "\"score\": 0," +
                "\"last_activity_date\": 1479060681," +
                "\"creation_date\": 1478998856," +
                "\"question_id\": 40569436," +
                "\"link\": \"http://stackoverflow.com/questions/40569436/kotlin-addtextchangelistener-lambda\"," +
                "\"title\": \"Kotlin addTextChangeListener lambda?\"" +
                "}" +
                "]," +
                "\"has_more\": true," +
                "\"quota_max\": 300," +
                "\"quota_remaining\": 289" +
                "}"

        assertEquals(correctResponse, response ?: "")
    }
}
