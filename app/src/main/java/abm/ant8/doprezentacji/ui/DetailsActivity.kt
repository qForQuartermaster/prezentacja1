package abm.ant8.doprezentacji.ui

import abm.ant8.doprezentacji.R
import abm.ant8.doprezentacji.model.AnswerForDetails
import abm.ant8.doprezentacji.model.AnswersWrapper
import abm.ant8.doprezentacji.model.ListItem
import abm.ant8.doprezentacji.model.QuestionWrapper
import abm.ant8.doprezentacji.ui.adapters.DetailsAdapter
import android.animation.ObjectAnimator
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.util.Log
import android.view.Gravity
import android.view.View.GONE
import android.view.animation.DecelerateInterpolator
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import java.net.URL
import java.net.URLDecoder
import java.net.URLEncoder


class DetailsActivity : AppCompatActivity() {

    lateinit var questionTitle: TextView
    lateinit var responseView: RecyclerView
    lateinit var horizontalProgressBar: ProgressBar
    var detailsList = mutableListOf<ListItem>()
    lateinit var animation: ObjectAnimator

    private val LOG_TAG = "prezka"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val questionId = intent.getLongExtra("questionId", 0)

        relativeLayout {
            horizontalProgressBar = progressBar {
                max = 500
                progress = 0
            }
            horizontalProgressBar.lparams(width = 150, height = 150) {
                gravity = Gravity.CENTER_HORIZONTAL or Gravity.CENTER_VERTICAL
            }
            linearLayout {
                orientation = LinearLayout.VERTICAL
                questionTitle = textView {
                    textSize = 24f
                    typeface = Typeface.DEFAULT_BOLD
                }
                responseView = recyclerView {
                    id = R.id.results_recycler_view
                    layoutManager = LinearLayoutManager(this.context)
                    adapter = DetailsAdapter(detailsList)
                }
            }
        }

        animation = ObjectAnimator.ofInt(horizontalProgressBar, "progress", 0, 500)
        animation.duration = 5000
        animation.interpolator = DecelerateInterpolator()
        animation.start()

        getQuestion(questionId)
        getAnswers(questionId)
    }

    private fun getAnswers(questionId: Long) {
        doAsync {
            var answers = ""
            try {
                answers = URL("https://api.stackexchange.com/2.2/questions/" +
                        "$questionId/answers?order=desc&sort=activity&site=stackoverflow" +
                        "&filter=!9YdnSMKKT").readText()
            } catch(e: Exception) {
                Log.d(LOG_TAG, "wyjątek to $e")
            }

            val resultsWrapper = jacksonObjectMapper().readValue<AnswersWrapper>(answers)
            detailsList.addAll(resultsWrapper.items)
            if (resultsWrapper.items.size < 1)
                detailsList.add(AnswerForDetails(0, 0, ctx.resources.getString(R.string.no_answers), false))

            uiThread {
                responseView.adapter.notifyDataSetChanged()
                animation.cancel()
                horizontalProgressBar.visibility = GONE
            }
        }
    }

    private fun getQuestion(questionId: Long) {
        doAsync {
            var response = ""
            try {
                response = URL("https://api.stackexchange.com/2.2/questions/" +
                        "$questionId?order=desc&sort=activity&site=stackoverflow" +
                        "&filter=!9YdnSIN18").readText()
            } catch(e: Exception) {
                Log.d(LOG_TAG, "wyjątek to $e")
            }

            val questionWrapper = jacksonObjectMapper().readValue<QuestionWrapper>(response)
            detailsList.add(0, questionWrapper.items[0])
            uiThread {
                responseView.adapter.notifyDataSetChanged()
                animation.cancel()
                horizontalProgressBar.visibility = GONE
                questionTitle.text = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N)
                                         Html.fromHtml(questionWrapper.items[0].title, Html.FROM_HTML_MODE_LEGACY)
                                     else Html.fromHtml(questionWrapper.items[0].title)
            }

        }
    }
}
