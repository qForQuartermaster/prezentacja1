package abm.ant8.doprezentacji.ui

import abm.ant8.doprezentacji.R
import abm.ant8.doprezentacji.model.AnswerForDetails
import abm.ant8.doprezentacji.model.QuestionForResults
import abm.ant8.doprezentacji.model.ResultsListWrapper
import abm.ant8.doprezentacji.ui.adapters.ResultsAdapter
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import java.net.URL
import java.net.URLEncoder

class ResultsActivity : AppCompatActivity() {
    lateinit var response: String
    lateinit var responseView: RecyclerView
    var resultsList = mutableListOf<QuestionForResults>()

    private val LOG_TAG = "prezka"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val bundle = this.intent.extras

        val searchText = bundle.getString("searchText")
        val tags = bundle.getString("tags")

//  todo https://medium.com/@BhaskerShrestha/kotlin-and-anko-for-your-android-1c11054dd255#.li8rsjbkx

        relativeLayout {
            responseView = recyclerView {
                id = R.id.results_recycler_view
                layoutManager = LinearLayoutManager(this.context)
                adapter = ResultsAdapter(resultsList) { startActivity<DetailsActivity>("questionId" to it.questionId) }
            }
        }

        getQuestions(tags, searchText)
    }

    private fun getQuestions(tags: String, searchText: String) {
        doAsync {
            try {
                val resultsPerPage = 12
                val encodedTags = URLEncoder.encode(tags, "UTF-8")
                val encodedSearchText = URLEncoder.encode(searchText, "UTF-8")
                val query = "https://api.stackexchange.com/2.2/search/advanced" +
                        "?page=1&pagesize=$resultsPerPage" +
                        "&order=desc&sort=relevance" +
                        "&tagged=$encodedTags&q=$encodedSearchText&site=stackoverflow"
                response = URL(query).readText()
            } catch(e: Exception) {
                Log.d(LOG_TAG, "wyjątek podczas pobierania zapytania to $e")
            }

            var resultsWrapper = ResultsListWrapper(emptyList())
            try {
                resultsWrapper = jacksonObjectMapper().readValue<ResultsListWrapper>(response)
            } catch(e: Exception) {
                Log.d(LOG_TAG, "wyjątek przy prasowaniu to $e")
            }
            resultsList.addAll(resultsWrapper.items)
            if (resultsWrapper.items.size < 1)
                resultsList.add(QuestionForResults(false, 0, 0, 0, 0, 0, ctx.resources.getString(R.string.no_answers)))

            uiThread {
                responseView.adapter.notifyDataSetChanged()
            }
        }
    }
}
