package abm.ant8.doprezentacji.ui.adapters

import abm.ant8.doprezentacji.R
import abm.ant8.doprezentacji.model.AnswerForDetails
import abm.ant8.doprezentacji.model.ListItem
import abm.ant8.doprezentacji.model.ListItem.Companion.ANSWER
import abm.ant8.doprezentacji.model.ListItem.Companion.QUESTION
import abm.ant8.doprezentacji.model.QuestionForDetails
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.Html.fromHtml
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import org.jetbrains.anko.*

class DetailsAdapter(val resultsList: List<ListItem>) : RecyclerView.Adapter<DetailsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): DetailsViewHolder? {
        return when (viewType) {
            QUESTION -> QuestionViewHolder(DetailsItemUI().createView(AnkoContext.Companion.create(parent!!.context, parent)))
            ANSWER -> AnswerViewHolder(DetailsItemUI().createView(AnkoContext.Companion.create(parent!!.context, parent)))
            else -> null
        }
    }

    override fun onBindViewHolder(holder: DetailsViewHolder?, position: Int) {
        val question = resultsList[position]
        holder!!.bind(question)
    }

    override fun getItemCount(): Int = resultsList.size

    override fun getItemViewType(position: Int): Int = resultsList[position].listItemType
}

class DetailsItemUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            linearLayout {
                lparams(width = matchParent)
                orientation = LinearLayout.HORIZONTAL
                linearLayout {
                    orientation = LinearLayout.VERTICAL
                    textView {
                        id = R.id.details_item_score
                        textSize = 16f
                    }
                }
                textView {
                    id = R.id.details_item_body
                    textSize = 16f
                }
            }
        }
    }
}


abstract class DetailsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun bind(item: ListItem)
}

class QuestionViewHolder(itemView: View) : DetailsViewHolder(itemView) {
    val body: TextView = itemView.find(R.id.details_item_body)
    val score: TextView = itemView.find(R.id.details_item_score)

    override fun bind(item: ListItem) {
        val bodyText = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N)
                                fromHtml((item as QuestionForDetails).body, Html.FROM_HTML_MODE_LEGACY)
                        else fromHtml((item as QuestionForDetails).body)
        body.text = "$bodyText"
        score.text = "${item.score}"
    }
}

class AnswerViewHolder(itemView: View) : DetailsViewHolder(itemView) {
    val body: TextView = itemView.find(R.id.details_item_body)
    val score: TextView = itemView.find(R.id.details_item_score)

    override fun bind(item: ListItem) {
        val bodyText = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N)
                           fromHtml((item as AnswerForDetails).body, Html.FROM_HTML_MODE_LEGACY)
                       else fromHtml((item as AnswerForDetails).body)

        body.text = "$bodyText"
        score.text = "${item.score}"
    }
}