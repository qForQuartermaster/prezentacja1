package abm.ant8.doprezentacji.ui.adapters

import abm.ant8.doprezentacji.R
import abm.ant8.doprezentacji.model.QuestionForResults
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.jetbrains.anko.*

class ResultsAdapter(val results: List<QuestionForResults>, val itemClickListener : (QuestionForResults) -> Unit) : RecyclerView.Adapter<ResultsItemViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ResultsItemViewHolder? {
        return ResultsItemViewHolder(ResultsItemUI().createView(AnkoContext.Companion.create(parent!!.context, parent)), itemClickListener)
    }

    override fun onBindViewHolder(holder: ResultsItemViewHolder?, position: Int) {
        val question = results[position]
        holder!!.bind(question)
    }

    override fun getItemCount(): Int {
        return results.size
    }
}

class ResultsItemUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            linearLayout {
                lparams(width = matchParent, height = dip(48))
//                orientation = LinearLayout.HORIZONTAL
                textView {
                    id = R.id.result_title
                    textSize = 16f
                }
            }
        }
    }
}


class ResultsItemViewHolder(itemView: View, val itemClickListener : (QuestionForResults) -> Unit) : RecyclerView.ViewHolder(itemView) {
    val title: TextView = itemView.find(R.id.result_title)

    fun bind(question: QuestionForResults) {
        title.text = "${question.answerCount} ${question.title}"
        itemView.onClick {
            itemClickListener(question) }
    }
}