package abm.ant8.doprezentacji.ui

//czy do prezentacji

import abm.ant8.doprezentacji.R
import abm.ant8.doprezentacji.ui.ResultsActivity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.EditText
import org.jetbrains.anko.*

class MainActivity : AppCompatActivity() {

    lateinit var searchEditText: EditText
    lateinit var searchTagsEditText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        verticalLayout {
            searchEditText = editText { hint = "słowa kluczowe" }
            searchTagsEditText = editText { hint = "tagi" }
//            czy szukaj? - og. polskie wyrażenia
            button(context.getString(R.string.search_button_caption)) {
                onClick {
                    val searchText = searchEditText.text.toString()
                    val tags = searchTagsEditText.text.toString()
                    startActivity<ResultsActivity>("searchText" to searchText, "tags" to tags.replace(",", ";"))
                }
            }
        }
    }
}
