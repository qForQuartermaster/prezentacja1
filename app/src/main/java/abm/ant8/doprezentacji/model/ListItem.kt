package abm.ant8.doprezentacji.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
abstract class ListItem(val score: Int,
               @JsonProperty("creation_date") val creationDate: Long,
               val body: String) {

    abstract val listItemType: Int

    companion object {
        val QUESTION = 1
        val ANSWER = 2
    }
}