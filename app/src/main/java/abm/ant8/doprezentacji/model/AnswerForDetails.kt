package abm.ant8.doprezentacji.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class AnswerForDetails(score: Int,
                       creationDate: Long,
                       body: String,
                       val accepted: Boolean)
    : ListItem(score, creationDate, body) {
    override val listItemType: Int
        get() = ANSWER
}