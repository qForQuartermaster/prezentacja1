package abm.ant8.doprezentacji.model

import abm.ant8.doprezentacji.model.QuestionForResults
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class ResultsListWrapper(val items: List<QuestionForResults>)