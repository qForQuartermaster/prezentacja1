package abm.ant8.doprezentacji.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class QuestionForResults(@JsonProperty("is_answered") val isAnswered: Boolean,
                              @JsonProperty("answer_count") val answerCount: Int,
                              @JsonProperty("last_activity_date") val lastActivityDate: Long,
                              @JsonProperty("question_id") val questionId: Long,
                              val score: Int,
                              @JsonProperty("creation_date") val creationDate: Long,
                              val title: String)