package abm.ant8.doprezentacji.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.net.URLDecoder

@JsonIgnoreProperties(ignoreUnknown = true)
class QuestionForDetails(score: Int,
                         creationDate: Long,
                         body: String,
                         @JsonProperty("is_answered") val answered: Boolean) : ListItem(score, creationDate, body) {
    override val listItemType: Int
        get() = QUESTION
    var title: String = ""
        set(value) { field = URLDecoder.decode(value, "UTF-8") }

}