package abm.ant8.doprezentacji.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class AnswersWrapper(val items: List<AnswerForDetails>)

@JsonIgnoreProperties(ignoreUnknown = true)
data class QuestionWrapper(val items: List<QuestionForDetails>)

