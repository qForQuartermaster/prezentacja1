package abm.ant8.doprezentacji

import abm.ant8.doprezentacji.model.*
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.Assert.*
import org.junit.Test

class JsonTest {
    @Test
    fun checkGameWrapperStartDate() {
        //            .setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE)
        val mapper = jacksonObjectMapper()

        val response = "{\"info\":{\"start_date\":\"2016-10-23T17:51:58.083667Z\",\"players\":[{\"position\":null,\"name\":\"ktokolwiek\",\"is_zombie\":true},{\"position\":{\"lng\":21.04383404881211,\"lat\":52.25450851962236},\"name\":\"Alfonso\",\"is_zombie\":true},{\"position\":{\"lng\":21.0438849,\"lat\":52.2547407},\"name\":\"Rafał\",\"is_zombie\":false},{\"position\":{\"lng\":20.990908899999997,\"lat\":52.2273978},\"name\":\"Dominika\",\"is_zombie\":true},{\"position\":{\"lng\":20.996557474136353,\"lat\":52.225295543775495},\"name\":\"Dupa\",\"is_zombie\":true},{\"position\":{\"lng\":20.99684715270996,\"lat\":52.25302744276857},\"name\":\"asd\",\"is_zombie\":false},{\"position\":{\"lng\":21.0438608,\"lat\":52.2546547},\"name\":\"Niedowidek\",\"is_zombie\":true},{\"position\":{\"lng\":21.043875099999998,\"lat\":52.25474320000001},\"name\":\"ghggfh\",\"is_zombie\":true},{\"position\":{\"lng\":21.043890599999997,\"lat\":52.254755599999996},\"name\":\"Szymon\",\"is_zombie\":true},{\"position\":null,\"name\":\"trueskawka\",\"is_zombie\":true},{\"position\":{\"lng\":20.9907474,\"lat\":52.2453384},\"name\":\"Nastya\",\"is_zombie\":true}],\"next_visible\":\"2016-10-23T18:35:36Z\",\"last_visible\":\"2016-10-23T18:34:36.870375Z\"}}"

        val responseStack = "{\"items\":[{\"tags\":[\"java\",\"generics\",\"binary-search-tree\"],\"owner\":{\"reputation\":928,\"user_id\":933162,\"user_type\":\"registered\",\"accept_rate\":90,\"profile_image\":\"https://i.stack.imgur.com/PAaRL.png?s=128&g=1\",\"display_name\":\"Md Faisal\",\"link\":\"http://stackoverflow.com/users/933162/md-faisal\"},\"is_answered\":true,\"view_count\":103,\"accepted_answer_id\":41319920,\"answer_count\":1,\"score\":-1,\"last_activity_date\":1482657165,\"creation_date\":1482562907,\"last_edit_date\":1482581667,\"question_id\":41311125,\"link\":\"http://stackoverflow.com/questions/41311125/implementing-iterator-in-binary-search-tree\",\"title\":\"Implementing iterator in Binary Search Tree\"},{\"tags\":[\"java\",\"eclipse\",\"arraylist\",\"iterator\"],\"owner\":{\"reputation\":8,\"user_id\":6936257,\"user_type\":\"registered\",\"profile_image\":\"https://graph.facebook.com/1155003917887589/picture?type=large\",\"display_name\":\"Aditya Paul\",\"link\":\"http://stackoverflow.com/users/6936257/aditya-paul\"},\"is_answered\":true,\"view_count\":42,\"accepted_answer_id\":41272606,\"answer_count\":2,\"score\":-1,\"last_activity_date\":1482357658,\"creation_date\":1482357458,\"question_id\":41272591,\"link\":\"http://stackoverflow.com/questions/41272591/using-an-iterator-twice-in-java\",\"title\":\"Using an iterator twice in java\"},{\"tags\":[\"java\",\"iterator\"],\"owner\":{\"reputation\":271,\"user_id\":261656,\"user_type\":\"registered\",\"accept_rate\":44,\"profile_image\":\"https://www.gravatar.com/avatar/da0ba33a2bf3f8e352450710693af526?s=128&d=identicon&r=PG\",\"display_name\":\"arindam\",\"link\":\"http://stackoverflow.com/users/261656/arindam\"},\"is_answered\":true,\"view_count\":4121,\"answer_count\":6,\"score\":1,\"last_activity_date\":1482302453,\"creation_date\":1264778899,\"question_id\":2162917,\"link\":\"http://stackoverflow.com/questions/2162917/can-we-use-for-each-loop-for-iterating-the-objects-of-iterator-type\",\"title\":\"Can we use for-each loop for iterating the objects of Iterator type?\"},{\"tags\":[\"java\",\"guava\",\"foreach\"],\"owner\":{\"reputation\":56070,\"user_id\":228171,\"user_type\":\"registered\",\"accept_rate\":78,\"profile_image\":\"https://i.stack.imgur.com/5T71u.png?s=128&g=1\",\"display_name\":\"Mark Peters\",\"link\":\"http://stackoverflow.com/users/228171/mark-peters\"},\"is_answered\":true,\"view_count\":8256,\"accepted_answer_id\":3883636,\"answer_count\":8,\"score\":28,\"last_activity_date\":1482302339,\"creation_date\":1286464521,\"last_edit_date\":1286466407,\"question_id\":3883131,\"link\":\"http://stackoverflow.com/questions/3883131/idiomatic-way-to-use-for-each-loop-given-an-iterator\",\"title\":\"Idiomatic way to use for-each loop given an iterator?\"},{\"tags\":[\"java\",\"iterator\",\"iterable\"],\"owner\":{\"reputation\":2846,\"user_id\":24028,\"user_type\":\"registered\",\"accept_rate\":56,\"profile_image\":\"https://www.gravatar.com/avatar/6e66bbbd727d279fe5237f6e11b0a5a0?s=128&d=identicon&r=PG\",\"display_name\":\"Łukasz Bownik\",\"link\":\"http://stackoverflow.com/users/24028/%c5%81ukasz-bownik\"},\"is_answered\":true,\"view_count\":36025,\"accepted_answer_id\":839192,\"answer_count\":14,\"score\":146,\"last_activity_date\":1482302155,\"creation_date\":1241777983,\"last_edit_date\":1351426884,\"question_id\":839178,\"link\":\"http://stackoverflow.com/questions/839178/why-is-javas-iterator-not-an-iterable\",\"title\":\"Why is Java&#39;s Iterator not an Iterable?\"}],\"has_more\":true,\"quota_max\":300,\"quota_remaining\":292}"
        val listWrapper = mapper.readValue<ResultsListWrapper>(responseStack)

        assertEquals(1241777983, listWrapper.items[4].creationDate)
//        assertTrue(gameWrapper.items[0].creationDate == "2016-10-23T17:51:58.083667Z")

    }

    @Test
    fun shouldQuestionBeDeserialised() {
        val response = "{\"tags\":[\"java\",\"iterator\",\"iterable\"],\"owner\":{\"reputation\":2846,\"user_id\":24028,\"user_type\":\"registered\",\"accept_rate\":56,\"profile_image\":\"https://www.gravatar.com/avatar/6e66bbbd727d279fe5237f6e11b0a5a0?s=128&d=identicon&r=PG\",\"display_name\":\"Łukasz Bownik\",\"link\":\"http://stackoverflow.com/users/24028/%c5%81ukasz-bownik\"},\"is_answered\":true,\"view_count\":36025,\"accepted_answer_id\":839192,\"answer_count\":14,\"score\":146,\"last_activity_date\":1482302155,\"creation_date\":1241777983,\"last_edit_date\":1351426884,\"question_id\":839178,\"link\":\"http://stackoverflow.com/questions/839178/why-is-javas-iterator-not-an-iterable\",\"title\":\"Why is Java&#39;s Iterator not an Iterable?\"}"

        val question = jacksonObjectMapper().readValue<QuestionForResults>(response)

        println(question.creationDate)
        assertEquals("Why is Java&#39;s Iterator not an Iterable?", question.title)
    }

    @Test
    fun shouldListOfQuestionsBeDeserialised() {
        val response = "[{\"tags\":[\"java\",\"generics\",\"binary-search-tree\"],\"owner\":{\"reputation\":928,\"user_id\":933162,\"user_type\":\"registered\",\"accept_rate\":90,\"profile_image\":\"https://i.stack.imgur.com/PAaRL.png?s=128&g=1\",\"display_name\":\"Md Faisal\",\"link\":\"http://stackoverflow.com/users/933162/md-faisal\"},\"is_answered\":true,\"view_count\":103,\"accepted_answer_id\":41319920,\"answer_count\":1,\"score\":-1,\"last_activity_date\":1482657165,\"creation_date\":1482562907,\"last_edit_date\":1482581667,\"question_id\":41311125,\"link\":\"http://stackoverflow.com/questions/41311125/implementing-iterator-in-binary-search-tree\",\"title\":\"Implementing iterator in Binary Search Tree\"},{\"tags\":[\"java\",\"eclipse\",\"arraylist\",\"iterator\"],\"owner\":{\"reputation\":8,\"user_id\":6936257,\"user_type\":\"registered\",\"profile_image\":\"https://graph.facebook.com/1155003917887589/picture?type=large\",\"display_name\":\"Aditya Paul\",\"link\":\"http://stackoverflow.com/users/6936257/aditya-paul\"},\"is_answered\":true,\"view_count\":42,\"accepted_answer_id\":41272606,\"answer_count\":2,\"score\":-1,\"last_activity_date\":1482357658,\"creation_date\":1482357458,\"question_id\":41272591,\"link\":\"http://stackoverflow.com/questions/41272591/using-an-iterator-twice-in-java\",\"title\":\"Using an iterator twice in java\"},{\"tags\":[\"java\",\"iterator\"],\"owner\":{\"reputation\":271,\"user_id\":261656,\"user_type\":\"registered\",\"accept_rate\":44,\"profile_image\":\"https://www.gravatar.com/avatar/da0ba33a2bf3f8e352450710693af526?s=128&d=identicon&r=PG\",\"display_name\":\"arindam\",\"link\":\"http://stackoverflow.com/users/261656/arindam\"},\"is_answered\":true,\"view_count\":4121,\"answer_count\":6,\"score\":1,\"last_activity_date\":1482302453,\"creation_date\":1264778899,\"question_id\":2162917,\"link\":\"http://stackoverflow.com/questions/2162917/can-we-use-for-each-loop-for-iterating-the-objects-of-iterator-type\",\"title\":\"Can we use for-each loop for iterating the objects of Iterator type?\"},{\"tags\":[\"java\",\"guava\",\"foreach\"],\"owner\":{\"reputation\":56070,\"user_id\":228171,\"user_type\":\"registered\",\"accept_rate\":78,\"profile_image\":\"https://i.stack.imgur.com/5T71u.png?s=128&g=1\",\"display_name\":\"Mark Peters\",\"link\":\"http://stackoverflow.com/users/228171/mark-peters\"},\"is_answered\":true,\"view_count\":8256,\"accepted_answer_id\":3883636,\"answer_count\":8,\"score\":28,\"last_activity_date\":1482302339,\"creation_date\":1286464521,\"last_edit_date\":1286466407,\"question_id\":3883131,\"link\":\"http://stackoverflow.com/questions/3883131/idiomatic-way-to-use-for-each-loop-given-an-iterator\",\"title\":\"Idiomatic way to use for-each loop given an iterator?\"},{\"tags\":[\"java\",\"iterator\",\"iterable\"],\"owner\":{\"reputation\":2846,\"user_id\":24028,\"user_type\":\"registered\",\"accept_rate\":56,\"profile_image\":\"https://www.gravatar.com/avatar/6e66bbbd727d279fe5237f6e11b0a5a0?s=128&d=identicon&r=PG\",\"display_name\":\"Łukasz Bownik\",\"link\":\"http://stackoverflow.com/users/24028/%c5%81ukasz-bownik\"},\"is_answered\":true,\"view_count\":36025,\"accepted_answer_id\":839192,\"answer_count\":14,\"score\":146,\"last_activity_date\":1482302155,\"creation_date\":1241777983,\"last_edit_date\":1351426884,\"question_id\":839178,\"link\":\"http://stackoverflow.com/questions/839178/why-is-javas-iterator-not-an-iterable\",\"title\":\"Why is Java&#39;s Iterator not an Iterable?\"}]"

        val list = jacksonObjectMapper().readValue<List<QuestionForResults>>(response)

        assertEquals(1241777983, list[4].creationDate)
    }

    @Test
    fun  shouldParseAnswersList() {
        var answers: String = "{\n" +
                "  \"items\": [\n" +
                "    {\n" +
                "      \"owner\": {\n" +
                "        \"reputation\": 1,\n" +
                "        \"user_id\": 5837222,\n" +
                "        \"user_type\": \"registered\",\n" +
                "        \"profile_image\": \"https://lh4.googleusercontent.com/-SBLz8ZNNAdY/AAAAAAAAAAI/AAAAAAAAABQ/1rdtE-SDFNY/photo.jpg?sz=128\",\n" +
                "        \"display_name\": \"Arun Kawdiya\",\n" +
                "        \"link\": \"http://stackoverflow.com/users/5837222/arun-kawdiya\"\n" +
                "      },\n" +
                "      \"is_accepted\": false,\n" +
                "      \"score\": 0,\n" +
                "      \"last_activity_date\": 1459494965,\n" +
                "      \"creation_date\": 1459494965,\n" +
                "      \"answer_id\": 36350223,\n" +
                "      \"question_id\": 11696521,\n" +
                "      \"body\": \"<p>pass a url from command line keep your url in app gradle file as follows\\n <strong>resValue \\\"string\\\", \\\"url\\\", CommonUrl</strong></p>\\n\\n<p>and give a parameter in gradle.properties files as follows \\n<strong>CommonUrl=\\\"put your url here or may be empty\\\"</strong></p>\\n\\n<p>and pass a command to from command line as follows\\n<strong>gradle assembleRelease -Pcommanurl=put your URL here</strong></p>\\n\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"owner\": {\n" +
                "        \"reputation\": 732,\n" +
                "        \"user_id\": 511828,\n" +
                "        \"user_type\": \"registered\",\n" +
                "        \"profile_image\": \"https://www.gravatar.com/avatar/4599ccfa67aa3f18ca284a8284bb160c?s=128&d=identicon&r=PG\",\n" +
                "        \"display_name\": \"Oscar Raig Colon\",\n" +
                "        \"link\": \"http://stackoverflow.com/users/511828/oscar-raig-colon\"\n" +
                "      },\n" +
                "      \"is_accepted\": false,\n" +
                "      \"score\": 12,\n" +
                "      \"last_activity_date\": 1427563351,\n" +
                "      \"creation_date\": 1427563351,\n" +
                "      \"answer_id\": 29320203,\n" +
                "      \"question_id\": 11696521,\n" +
                "      \"body\": \"<p>My program with two arguments, args[0] and args[1]:</p>\\n\\n<pre><code>public static void main(String[] args) throws Exception {\\n    System.out.println(args);\\n    String host = args[0];\\n    System.out.println(host);\\n    int port = Integer.parseInt(args[1]);\\n</code></pre>\\n\\n<p>my build.gradle</p>\\n\\n<pre><code>run {\\n    if ( project.hasProperty(\\\"appArgsWhatEverIWant\\\") ) {\\n        args Eval.me(appArgsWhatEverIWant)\\n    }\\n}\\n</code></pre>\\n\\n<p>my terminal prompt:</p>\\n\\n<pre><code>gradle run  -PappArgsWhatEverIWant=\\\"['localhost','8080']\\\"\\n</code></pre>\\n\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"owner\": {\n" +
                "        \"reputation\": 2071,\n" +
                "        \"user_id\": 411282,\n" +
                "        \"user_type\": \"registered\",\n" +
                "        \"accept_rate\": 74,\n" +
                "        \"profile_image\": \"https://www.gravatar.com/avatar/14d0a8831828a8d4ffe2f78964cc0332?s=128&d=identicon&r=PG\",\n" +
                "        \"display_name\": \"Joshua Goldberg\",\n" +
                "        \"link\": \"http://stackoverflow.com/users/411282/joshua-goldberg\"\n" +
                "      },\n" +
                "      \"is_accepted\": false,\n" +
                "      \"score\": 35,\n" +
                "      \"last_activity_date\": 1381261681,\n" +
                "      \"creation_date\": 1381261681,\n" +
                "      \"answer_id\": 19256949,\n" +
                "      \"question_id\": 11696521,\n" +
                "      \"body\": \"<p>Building on Peter N's answer, this is an example of how to add (optional) user-specified arguments to pass to Java main for a JavaExec task (since you can't set the 'args' property manually for the reason he cites.)</p>\\n\\n<p>Add this to the task:</p>\\n\\n<pre><code>task(runProgram, type: JavaExec) {\\n\\n  [...]\\n\\n  if (project.hasProperty('myargs')) {\\n      args(myargs.split(','))\\n  }\\n</code></pre>\\n\\n<p>... and run at the command line like this</p>\\n\\n<pre><code>% ./gradlew runProgram '-Pmyargs=-x,7,--no-kidding,/Users/rogers/tests/file.txt'\\n</code></pre>\\n\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"owner\": {\n" +
                "        \"reputation\": 72165,\n" +
                "        \"user_id\": 84889,\n" +
                "        \"user_type\": \"registered\",\n" +
                "        \"profile_image\": \"https://www.gravatar.com/avatar/8d9261be2c7926796bf37c7155133aec?s=128&d=identicon&r=PG\",\n" +
                "        \"display_name\": \"Peter Niederwieser\",\n" +
                "        \"link\": \"http://stackoverflow.com/users/84889/peter-niederwieser\"\n" +
                "      },\n" +
                "      \"is_accepted\": true,\n" +
                "      \"score\": 39,\n" +
                "      \"last_activity_date\": 1343429450,\n" +
                "      \"creation_date\": 1343429450,\n" +
                "      \"answer_id\": 11696629,\n" +
                "      \"question_id\": 11696521,\n" +
                "      \"body\": \"<p><code>project.group</code> is a predefined property. With <code>-P</code>, you can only set project properties that are <em>not</em> predefined. Alternatively, you can set Java system properties (<code>-D</code>).</p>\\n\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"has_more\": false,\n" +
                "  \"quota_max\": 10000,\n" +
                "  \"quota_remaining\": 9977\n" +
                "}"

        val list = jacksonObjectMapper().readValue<AnswersWrapper>(answers)

        assertFalse(list.items[1].accepted)
    }

    @Test
    fun shouldParseQuestion() {
        var question: String = "{\n" +
                "  \"items\": [\n" +
                "    {\n" +
                "      \"tags\": [\n" +
                "        \"gradle\"\n" +
                "      ],\n" +
                "      \"owner\": {\n" +
                "        \"reputation\": 445,\n" +
                "        \"user_id\": 1143956,\n" +
                "        \"user_type\": \"registered\",\n" +
                "        \"accept_rate\": 75,\n" +
                "        \"profile_image\": \"https://www.gravatar.com/avatar/4043794ef7f6e43314432f2177407942?s=128&d=identicon&r=PG\",\n" +
                "        \"display_name\": \"Lidia\",\n" +
                "        \"link\": \"http://stackoverflow.com/users/1143956/lidia\"\n" +
                "      },\n" +
                "      \"is_answered\": true,\n" +
                "      \"view_count\": 62078,\n" +
                "      \"accepted_answer_id\": 11696629,\n" +
                "      \"answer_count\": 4,\n" +
                "      \"score\": 53,\n" +
                "      \"last_activity_date\": 1459494965,\n" +
                "      \"creation_date\": 1343428441,\n" +
                "      \"last_edit_date\": 1354207656,\n" +
                "      \"question_id\": 11696521,\n" +
                "      \"link\": \"http://stackoverflow.com/questions/11696521/how-to-pass-arguments-from-command-line-to-gradle\",\n" +
                "      \"title\": \"How to pass arguments from command line to gradle\",\n" +
                "      \"body\": \"<p>I'm trying to pass an argument from command line to a java class.  I followed this post: <a href=\\\"http://gradle.1045684.n5.nabble.com/Gradle-application-plugin-question-td5539555.html\\\">http://gradle.1045684.n5.nabble.com/Gradle-application-plugin-question-td5539555.html</a> but the code does not work for me (perhaps it is not meant for JavaExec?).  Here is what I tried:</p>\\n\\n<pre><code>task listTests(type:JavaExec){\\n    main = \\\"util.TestGroupScanner\\\"\\n    classpath = sourceSets.util.runtimeClasspath\\n    // this works...\\n    args 'demo'\\n    /*\\n    // this does not work!\\n    if (project.hasProperty(\\\"group\\\")){\\n        args group\\n    }\\n    */\\n}\\n</code></pre>\\n\\n<p>The output from the above hard coded args value is:</p>\\n\\n<pre><code>C:\\\\ws\\\\svn\\\\sqe\\\\sandbox\\\\selenium2forbg\\\\testgradle&gt;g listTests\\n:compileUtilJava UP-TO-DATE\\n:processUtilResources UP-TO-DATE\\n:utilClasses UP-TO-DATE\\n:listTests\\nReceived argument: demo\\n\\nBUILD SUCCESSFUL\\n\\nTotal time: 13.422 secs\\n</code></pre>\\n\\n<p>However, once I change the code to use the hasProperty section and pass \\\"demo\\\" as an argument on the command line, I get a NullPointerException:</p>\\n\\n<pre><code>C:\\\\ws\\\\svn\\\\sqe\\\\sandbox\\\\selenium2forbg\\\\testgradle&gt;g listTests -Pgroup=demo -s\\n\\nFAILURE: Build failed with an exception.\\n\\n* Where:\\nBuild file 'C:\\\\ws\\\\svn\\\\sqe\\\\sandbox\\\\selenium2forbg\\\\testgradle\\\\build.gradle' line:25\\n\\n* What went wrong:\\nA problem occurred evaluating root project 'testgradle'.\\n&gt; java.lang.NullPointerException (no error message)\\n\\n* Try:\\nRun with --info or --debug option to get more log output.\\n\\n* Exception is:\\norg.gradle.api.GradleScriptException: A problem occurred evaluating root project\\n 'testgradle'.\\n    at org.gradle.groovy.scripts.internal.DefaultScriptRunnerFactory\$ScriptRunnerImpl.run(DefaultScriptRunnerFactory.java:54)\\n    at org.gradle.configuration.DefaultScriptPluginFactory\$ScriptPluginImpl.apply(DefaultScriptPluginFactory.java:127)\\n    at org.gradle.configuration.BuildScriptProcessor.evaluate(BuildScriptProcessor.java:38) \\n</code></pre>\\n\\n<p>There is a simple test project available at <a href=\\\"http://gradle.1045684.n5.nabble.com/file/n5709919/testgradle.zip\\\">http://gradle.1045684.n5.nabble.com/file/n5709919/testgradle.zip</a> that illustrates the problem.</p>\\n\\n<p>This is using Gradle 1.0-rc-3.  The NullPointer is from this line of code:</p>\\n\\n<pre><code>args group \\n</code></pre>\\n\\n<p>I added the following assignment before the task definition, but it didn't change the outcome:</p>\\n\\n<pre><code>group = hasProperty('group') ? group : 'nosuchgroup' \\n</code></pre>\\n\\n<p>Any pointers on how to pass command line arguments to gradle appreciated.</p>\\n\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"has_more\": false,\n" +
                "  \"quota_max\": 10000,\n" +
                "  \"quota_remaining\": 9977\n" +
                "}"

        val listWithQuestion = jacksonObjectMapper().readValue<QuestionWrapper>(question)

        assertTrue(listWithQuestion.items[0].answered)
    }
}
